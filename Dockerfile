FROM ubuntu:noble

SHELL ["/bin/bash", "-c"]

ENV TZ=Etc/UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get -yqq update; \
    apt-get -yqq install apt-utils software-properties-common; \
    apt-get -yqq update; \
    apt-get -yqq dist-upgrade --allow-downgrades --allow-remove-essential --allow-change-held-packages; \
    apt-get -yqq install curl build-essential git unzip gnupg2 locales docutils-common; \
    apt-get -yqq install build-essential zlib1g-dev libffi-dev libssl-dev libbz2-dev libreadline-dev libsqlite3-dev liblzma-dev libncurses-dev; \
    apt-get -yqq install python3 python3-dev python3-venv python3-poetry; \
    apt-get -yqq install golang-go nodejs npm pip nox; \
    add-apt-repository --yes ppa:kicad/kicad-8.0-releases; \
    apt-get -yqq update; \
    apt-get -yqq install kicad; \
    apt-get -yqq install docker.io docker-compose-v2 docker-buildx; \
    apt-get -yqq install --no-install-recommends libatk-bridge2.0-0 libatk1.0-0 libatspi2.0-0 libcairo2 libcups2 libdbus-1-3 libdrm2 libgbm1 \
    libglib2.0-0 libnspr4 libnss3 libpango-1.0-0 libwayland-client0 libx11-6 libxcb1 libxcomposite1 libxdamage1 libxext6 libxfixes3 libxkbcommon0 \
    libxrandr2 xvfb fonts-noto-color-emoji fonts-unifont libfontconfig1 libfreetype6 xfonts-cyrillic xfonts-scalable fonts-liberation fonts-ipafont-gothic \
    fonts-wqy-zenhei fonts-tlwg-loma-otf fonts-freefont-ttf ffmpeg libcairo-gobject2 libdbus-glib-1-2 libgdk-pixbuf-2.0-0 libgtk-3-0 libpangocairo-1.0-0 \
    libx11-xcb1 libxcb-shm0 libxcursor1 libxi6 libxrender1 libxtst6 libsoup-3.0-0 libenchant-2-2 gstreamer1.0-libav gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-base gstreamer1.0-plugins-good libicu74 libegl1 libepoxy0 libevdev2 libgles2 libglx0 libgstreamer-gl1.0-0 \
    libgstreamer-plugins-base1.0-0 libgstreamer1.0-0 libgudev-1.0-0 libharfbuzz-icu0 libharfbuzz0b libhyphen0 libjpeg-turbo8 liblcms2-2 libmanette-0.2-0 \
    libnotify4 libopengl0 libopenjp2-7 libopus0 libpng16-16 libproxy1v5 libsecret-1-0 libwayland-egl1 libwayland-server0 libwebpdemux2 libwoff1 libxml2 \
    libxslt1.1 libx264-164 libatomic1 libevent-2.1-7;

# install pyenv
RUN git clone --depth=1 https://github.com/pyenv/pyenv.git .pyenv
ENV PYENV_ROOT="/.pyenv"
ENV PATH="${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:${PATH}"
# install different python versions; used for compatability tests
ENV PYTHON_VERSIONS="3.10.13 3.12.0"
RUN pyenv install $PYTHON_VERSIONS && pyenv global $PYTHON_VERSIONS

# make `pcbnew` available in all python versions
ENV SITE_PACKAGES="/.pyenv/versions/3.10.13/lib/python3.10/site-packages/ /.pyenv/versions/3.12.0/lib/python3.12/site-packages/"

RUN xargs -n 1 cp /usr/lib/python3/dist-packages/_pcbnew.so<<<"$SITE_PACKAGES"; \
    xargs -n 1 cp /usr/lib/python3/dist-packages/pcbnew.py<<<"$SITE_PACKAGES";

# install vale
RUN curl -L -o vale_3.6.1_Linux_64-bit.tar.gz https://github.com/errata-ai/vale/releases/download/v3.6.1/vale_3.6.1_Linux_64-bit.tar.gz; \
    mkdir vale && tar -xvzf vale_3.6.1_Linux_64-bit.tar.gz -C vale; \
    mv vale/vale /usr/local/bin/vale; \
    rm -rf vale vale_3.6.1_Linux_64-bit.tar.gz;

RUN useradd -m builder

USER builder

