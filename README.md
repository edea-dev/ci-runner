# EDeA CI Runner

This is a docker image for the EDeA test runners. It already contains all the needed utilities and packages so
that the CI tests can run much faster.
